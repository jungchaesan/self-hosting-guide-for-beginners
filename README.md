# A BEGINNER'S GUIDE TO SELF-HOSTING
***Edition 2022.1***

*A guide to hosting your own free and open source cloud, e-mail, instant messaging and video conferencing services.*

# Table of Contents

1. [Introduction](chapter1.md)
2. [Self-Hosting - Getting Started](chapter2-1.md)
<br>2.1. [Preliminary Considerations](chapter2-1.md)
<br>2.2. [Principle Components](chapter2-2.md)
3. [A Self-Hosted Setup using Docker and Traefik](chapter3-1.md)
<br>3.1. [Overview](chapter3-1.md)
<br>3.2. [Install Operating System and Container Engine](chapter3-2.md)
<br>3.3. [Traefik Reverse Proxy](chapter3-3.md)
<br>3.4. [Self-Hosted E-mail using Mailcow](chapter3-4.md)
<br>3.5. [Self-Hosted Instant Messaging, Audio and Video Calls using Matrix](chapter3-5.md)
<br>3.6. [Self-Hosted Video Conferencing using Jitsi](chapter3-6.md)
<br>3.7. [Self-Hosted File Cloud using Nextcloud](chapter3-7.md)
<br>3.8. [Automated Software Upgrades](chapter3-8.md)
4. [External Links](chapter4.md)

## Foreword

This guide describes how to set up a server for the purpose of self-hosting services such as e-mail, instant messaging, video conferencing and cloud file storage.

## License

The entire text of this guide and the associated shell scripts are released to the public under the terms of the [MIT license](https://mit-license.org/).
