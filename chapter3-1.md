\- [start](README.md) - [previous](chapter2-2.md) - [next](chapter3-2.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 1: Overview

The aim of this chapter is to present full technical instructions for setting up a homeserver for the purpose of self-hosting the following services using only free and open source software.

* Instant messaging.
* Video conferencing.
* Cloud file storage.
* E-mail.

The basic technology stack implemented by the server setup documented in this chapter is summarised in the table below.

|Category|Selection|Alternatives|
| --- | --- | --- |
|Architecture|x86-64|ARM64|
|Operating System|Debian|Any other Debian or Arch Linux based distribution e.g. Ubuntu Server, Manjaro.|
|Container Engine|Docker|Podman|
|Reverse Proxy|Traefik|Nginx|

The instructions presented in this chapter are thereby only valid for the x86-64 instruction set. They would need to be modified somewhat for devices such as the Raspberry Pi which utilise the ARM64 instruction set.

The following software has been selected to implement each respective service.

|Service|Software|
--- | --- |
|E-mail|Mailcow|
|Video conferencing|Jitsi|
|Instant messaging|Synapse (Matrix)|
|Cloud|Nextcloud|

Each service consists of multiple software components, with each component running inside its own Docker container. Docker Compose is used to coordinate the component micro-services which constitute a full service. A reverse proxy implemented using Traefik maps each service to a unique subdomain.

The reader needn't limit themselves to the services or the specific software listed above. The setup procedure which is described is quite general and may be applied to a wide range of software suitable for self-hosting. Within each category of service, various alternatives to the ones which we have selected might be considered by the reader, for example XMPP instead of Matrix. Before starting, it will be well worth your while to thoroughly research the various options taking into consideration your individual requirements.

Achieving a literal implementation of the provided instructions is likely to take you as little as half a day. It has been aimed to present as simple a server configuration as possible. The strengths of the specified setup are the completeness and usability of the chosen software, the modularity of the overall setup and the ease of setup and maintenance, however this setup does not represent a resource optimal solution and lacks some of the security features found in more complex offerings such as the integrated solutions mentioned in the previous chapter.

A server running all of the services listed above for a single user may be expected to require about 6-8 GB of RAM. Good performance has been reported with a 2.4 GHz clock rate, dual CPU setup but you might well get away with less processing power.

The instructions were tested using Debian 10. They are expected to be generally applicable for all similar Linux distributions, though here and there steps may vary.

Throughout this chapter, those fields inside configuration files or commands which you must change are annotated by a comment in CAPITAL LETTERS. Occasionally you may need to delete these comments for a configuration file to be valid.

If you don't already use a password manager, it is highly recommended that you install one on your work station, as you will have to create many strong passwords through the course of this tutorial.

On account of working primarily with Docker, the instructions throughout this chapter will generally be executed while in sudo interactive mode.

\- [start](README.md) - [previous](chapter2-2.md) - [next](chapter3-2.md) - [end](chapter4.md) -
