\- [start](README.md) - [previous](chapter1.md) - [next](chapter2-2.md) - [end](chapter4.md) -

# Chapter 2: Self-Hosting - Getting Started

## Section 1: Preliminary Considerations

This section outlines some preparatory steps necessary to get started with self-hosting.

Firstly you must decide which kinds of service you wish to self-host. Plausible candidates include video conferencing, instant messaging, e-mail and cloud file storage. Then find an open source software package which is actively maintained and which implements a feature list to your liking for each category of service which you wish to self-host. Pay close attention to how the software under consideration handles any data. Is user data stored on a centralised server or does it reside in your hands on your own server? Is all traffic encrypted and where are the encryption keys stored? These are examples of the kinds of question to consider while researching your options.

Next you must decide where you would like to keep your server. The most secure location benefiting from a stable and fast internet connection to which you have access might be your home. It might be an area inside a community building such as a church or voluntary association, set aside for its members for this purpose. If you frequently travel, you might prefer to rent a virtual private server (VPS) instead of buying a server which will be physically in your possession. You might even wish to consider running the services on which you rely on multiple servers in multiple locations, so that you will remain online even if a server in one location will fail, for example due to an internet outage or hardware failure. Distributing a service across multiple server instances may be accomplished using technologies such as Docker and Kubernetes.

Before purchasing hardware or subscribing to a VPS, you will need to estimate the hardware resource requirements such as RAM, storage capacity and processing power necessary to run the services which you intend to self-host. If you intend to self-host from home, you must also verify that your internet speed is sufficient for this purpose and that no ports to which you will require access have been blocked by your ISP (this is primarily relevant if you wish to self-host your own e-mail service).

Finally you must purchase a domain name. It is generally possible to host multiple services under a single domain name by assigning each service to a different sub-domain. Your video conferencing service might be hosted at meet.example.com and your cloud file storage service might be hosted at cloud.example.com. Occasionally a service (e.g. Matrix) might warrant an entire domain name to itself to mitigate against the possibility of cross-site scripting (XSS) attacks.

### Resources

|URL|Description|
| --- | --- |
|https://gitea.it/filippodb/applicazioni-self-hosting|An enormous list of free and open source software suitable for self-hosting.|
|https://www.opensourcealternative.to|Specifies open source alternatives to a variety of widely used commercial software, some of which are suitable for self-hosting.|

\- [start](README.md) - [previous](chapter1.md) - [next](chapter2-2.md) - [end](chapter4.md) -
