\- [start](README.md) - [previous](chapter3-8.md) -

# Chapter 4: External Links

In this chapter some useful external links are provided.

## Official Documentation

* https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker
* https://mailcow.github.io/mailcow-dockerized-docs
* https://matrix-org.github.io/synapse/latest/welcome_and_overview.html
* https://doc.traefik.io/traefik
* https://docs.nextcloud.com

## Lists and Directories

|URL|Description|
--- | --- |
|https://gitea.it/filippodb/applicazioni-self-hosting|An enormous list of free and open source software suitable for self-hosting.|
|https://www.opensourcealternative.to|Specifies open source alternatives to a variety of widely used commercial software, some of which are suitable for self-hosting.|
| https://www.gnu.org/distros/free-distros.html | The Free Software Foundation's list of endorsed operating systems. |

## Hardware Manufacturers

|URL|Description|
| --- | --- |
| https://www.pine64.org | A manufacturer of hardware for electronics and free software enthusiasts. |
| https://puri.sm | A US based hardware and software company with a strong free software ethos. |

## Integrated Homeserver Solutions

|URL|Description|
| --- | --- |
| https://gitlab.com/cyber5k/mistborn | A free and open source homeserver solution integrating a number of advanced security features. |
| https://freedombox.org | An integrated free and open source homeserver solution. |
| https://umbrel.com | An integrated open source homeserver solution which at the time of writing is under development. |

## Guides

|URL|Description|
--- | --- |
|https://goneuland.de|German language tutorials about setting up Traefik, Mailcow, Jitsi, Nextcloud and more.|
|https://www.youtube.com/watch?v=4rzc0hWRSPg|The Digital Life channel on Youtube contains video tutorials about home networks and related themes.|
|https://github.com/b-venter/Matrix-Docker-install|A tutorial about deploying a Synapse (Matrix) server using Docker.|
|https://autoize.ch/pgp-email-encryption-with-afterlogic-webmail-and-mailcow|A tutorial about integrating Afterlogic Webmail with Mailcow.|

\- [start](README.md) - [previous](chapter3-8.md) -
