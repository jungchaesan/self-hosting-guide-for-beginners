\- [start](README.md) - [previous](chapter3-2.md) - [next](chapter3-4.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 3: Setting up a Reverse Proxy using Traefik

In this section we deploy the Traefik open source edge router to implement a reverse proxy so as to assign each of the services which will be deployed in the subsequent sections to a distinct subdomain.

<table><tr><td>
<figure>
<p align="center">
<img src="images/traefik.png" alt="Traefik" width="75%"/>
</p>
</figure>
</td></tr></table>
<figcaption>Fig. 3.3.1: Generic Traefik schema. Source: <a href="https://github.com/traefik/traefik">https://github.com/traefik/traefik</a> (MIT License).</figcaption>

<br>Each service will be mapped to a distinct subdomain as follows.

|Service|Subdomain|
|---|---|
|Mailcow|mail.example.com|
|Matrix (Synapse)|synapse.example.net, example.net|
|Jitsi|jitsi.example.com|
Nextcloud|cloud.example.com|

Accordingly a user's e-mail address will take the form `user@example.com` and their Matrix ID will take the form `@user:example.net`.

### Configure DNS Records

Enter the administration panel where you administrate your DNS records. Create an A name record, mapping a valid domain name which you have purchased to the public IP address of your server. If your server does not benefit from a static IP address, you will have to use Dynamic DNS instead.

Create a CNAME record which maps a fully qualified domain name e.g. traefik.example.com to your valid domain name e.g. example.com.

### Setting Up Traefik

Start an interactive sudo session.

```
sudo -i
```

Install the htpasswd tool which will be used to create a string to securely store a password for Traefik later.

```
apt-get update
apt-get install apache2-utils
```

Create the following files and directories.

```
mkdir -p /opt/containers/traefik/data
cd /opt/containers/traefik

touch /opt/containers/traefik/docker-compose.yml
touch /opt/containers/traefik/data/traefik.yml
touch /opt/containers/traefik/data/dynamic_conf.yml

touch /opt/containers/traefik/data/acme.json
chmod 600 /opt/containers/traefik/data/acme.json
```

Open `/opt/containers/traefik/data/dynamic_conf.yml` for editing and insert the following content.

```
tls:
  options:
    default:
      minVersion: VersionTLS12
      cipherSuites:
        - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
        - TLS_AES_128_GCM_SHA256
        - TLS_AES_256_GCM_SHA384
        - TLS_CHACHA20_POLY1305_SHA256
      curvePreferences:
        - CurveP521
        - CurveP384
      sniStrict: true
http:
  middlewares:
    secHeaders:
      headers:
        browserXssFilter: true
        contentTypeNosniff: true
        frameDeny: true
        sslRedirect: true
        #HSTS Configuration
        stsIncludeSubdomains: true
        stsPreload: true
        stsSeconds: 31536000
        customFrameOptionsValue: "SAMEORIGIN"
```

Open `/opt/containers/traefik/data/traefik.yml` for editing and insert the following content.

```
api:
  dashboard: true
entryPoints:
  http:
    address: ":80"
  https:
    address: ":443"
providers:
  docker:
    endpoint: "unix:///var/run/docker.sock"
    exposedByDefault: false
  file:
    filename: "./dynamic_conf.yml"
certificatesResolvers:
  http:
    acme:
      email: email@example.com  # INSERT YOUR E-MAIL ADDRESS
      storage: acme.json
      httpChallenge:
        entryPoint: http
```

Create a string to securely store a username and password by running the following command, replacing "user" and "password" with values of your choice. The output of this command will be utilised shortly.

```
echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g
```

Open `/opt/containers/traefik/docker-compose.yml` for editing and insert the following content.

Replace "insert_password_string_here" with the output of the htpasswd command which you ran earlier e.g. `user:$$apr1$$7ciH.Uzt$$DUqDgmpLmFE2Fm7Z7VaMe/`.

```
version: '2'

services:
  traefik:
    image: traefik:latest
    container_name: traefik
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    networks:
      - proxy
    ports:
      - 80:80
      - 443:443
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /opt/containers/traefik/data/traefik.yml:/traefik.yml:ro
      - /opt/containers/traefik/data/acme.json:/acme.json
      - /opt/containers/traefik/data/dynamic_conf.yml:/dynamic_conf.yml
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.traefik.entrypoints=http"
      - "traefik.http.routers.traefik.rule=Host(`traefik.example.com`)"  # REPLACE WITH VALID SUBDOMAIN NAME
      - "traefik.http.middlewares.traefik-auth.basicauth.users=insert_password_string_here"  # UPDATE WITH HTPASSWD PASSWORD OUTPUT
      - "traefik.http.middlewares.traefik-https-redirect.redirectscheme.scheme=https"
      - "traefik.http.routers.traefik.middlewares=traefik-https-redirect"
      - "traefik.http.routers.traefik-secure.entrypoints=https"
      - "traefik.http.routers.traefik-secure.rule=Host(`traefik.example.com`)"  # REPLACE WITH VALID SUBDOMAIN NAME
      - "traefik.http.routers.traefik-secure.tls=true"
      - "traefik.http.routers.traefik-secure.tls.certresolver=http"
      - "traefik.http.routers.traefik-secure.service=api@internal"
      - "providers.file.filename=/dynamic_conf.yml"
      - "traefik.http.routers.traefik-secure.middlewares=secHeaders@file,traefik-auth"
networks:
  proxy:
    external: true
```

Create a Docker network and then build the Docker container. Wait for half a minute or so before entering the command to review the resulting log, as it may take a little while for the container to start.

```
docker network create proxy
docker-compose -f /opt/containers/traefik/docker-compose.yml up -d
docker-compose logs
```

Navigate to "traefik.example.com" in your web browser. The page should load a login form. Enter the username and password which you created earlier. After logging in successfully, the Traefik dashboard should be displayed.

### SSL Security Check

You can perform an SSL security check by entering traefik.example.com at https://www.ssllabs.com/ssltest/. The expected overall rating is A+.

### Maintenance

#### Performing a Manual Software Update

To keep your Traefik instance up to date, you must pull the latest image for the corresponding tag.

|Image|Tag|
--- | --- |
|traefik|latest|

To perform a manual update, first take note of the version which is currently deployed, in case you may wish to roll back. This information is displayed at the top of the page at "traefik.example.com". Then run the following commands.

```
cd /opt/containers/traefik
docker-compose pull
docker-compose up -d --remove-orphans

# Optionally remove obsolete images
docker image prune
```

See the section [Automated Software Updates](chapter3-8.md) for instructions about automating the update process.

\- [start](README.md) - [previous](chapter3-2.md) - [next](chapter3-4.md) - [end](chapter4.md) -
