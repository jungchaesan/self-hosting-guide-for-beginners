\- [start](README.md) - [previous](chapter3-3.md) - [next](chapter3-5.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 4: Self-Hosted E-mail using Mailcow

This section describes how to set up a Mailcow e-mail server and web client.

### Configure DNS Records

Enter the administration panel where you administrate your DNS records. It is assumed that you have already created an A record pointing your valid registered domain name e.g. example.com to the IP address of your server.

Additionally you must create 4 CNAME records, pointing the following subdomains to the domain name contained inside the A record.

* mail.example.com
* imap.example.com
* smtp.example.com
* pop3.example.com

### Setting Up Mailcow

It is assumed that you have already performed basic system configuration as described in [section 2](chapter3-2.md) and that you have set up Traefik as described in [section 3](chapter3-3.md).

Start an interactive sudo session and change into the directory where you keep your docker containers.

```
sudo -i
cd /opt/containers
mkdir mailcow
```

Clone the repository of Mailcow for Docker.

```
git clone https://github.com/mailcow/mailcow-dockerized /opt/containers/mailcow
```

Change into the `mailcow` directory. Run the following script to generate some configuration files. Enter your fully qualified domain name e.g. mail.example.com when prompted.

```
cd mailcow
./generate_config.sh
```

Open the "docker-compose.yaml" file for editing.

```
nano /opt/containers/mailcow/docker-compose.yml
```

Comment out the following lines.

```
      #ports:
        #- "${HTTPS_BIND:-0.0.0.0}:${HTTPS_PORT:-443}:${HTTPS_PORT:-443}"
        #- "${HTTP_BIND:-0.0.0.0}:${HTTP_PORT:-80}:${HTTP_PORT:-80}"
```

Create the following configuration file and open it for editing.

```
nano /opt/containers/mailcow/docker-compose.override.yml
```

Insert the following contents.

```
version: '2.1'

services:
    nginx-mailcow:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.nginx-mailcow.entrypoints=http"
        - "traefik.http.routers.nginx-mailcow.rule=HostRegexp(`{host:(autodiscover|autoconfig|webmail|mail|email).+}`)"
        - "traefik.http.middlewares.nginx-mailcow-https-redirect.redirectscheme.scheme=https"
        - "traefik.http.routers.nginx-mailcow.middlewares=nginx-mailcow-https-redirect"
        - "traefik.http.routers.nginx-mailcow-secure.entrypoints=https"
        - "traefik.http.routers.nginx-mailcow-secure.rule=Host(`mail.example.com`)"  # INSERT A VALID FULLY QUALIFIED DOMAIN NAME HERE
        - "traefik.http.routers.nginx-mailcow-secure.tls=true"
        - "traefik.http.routers.nginx-mailcow-secure.tls.domains[0].main=mail.example.com"  # INSERT A VALID FULLY QUALIFIED DOMAIN NAME HERE
        - "traefik.http.routers.nginx-mailcow-secure.tls.domains[0].sans=imap.example.com, smtp.example.com, pop3.example.com" # INSERT VALID SUBDOMAIN NAMES HERE
        - "traefik.http.routers.nginx-mailcow-secure.service=nginx-mailcow"
        - "traefik.http.services.nginx-mailcow.loadbalancer.server.port=80"
        - "traefik.docker.network=proxy"
        - "traefik.http.routers.nginx-mailcow-secure.middlewares=secHeaders@file"
        - "traefik.http.routers.nginx-mailcow-secure.tls.certresolver=http"
      networks:
        proxy:
    certdumper:
        image: humenius/traefik-certs-dumper
        container_name: traefik_certdumper
        restart: unless-stopped
        network_mode: none
        command: --restart-containers mailcowdockerized_postfix-mailcow_1,mailcowdockerized_dovecot-mailcow_1
        volumes:
          # mount the folder which contains Traefik's `acme.json' file
          #   in this case Traefik is started from its own docker-compose in ../traefik
          - /opt/containers/traefik/data:/traefik:ro
          # mount mailcow's SSL folder
          - /var/run/docker.sock:/var/run/docker.sock:ro
          - ./data/assets/ssl:/output:rw
        environment:
          # only change this, if you're using another domain for mailcow's web frontend compared to the standard config
          - DOMAIN=${MAILCOW_HOSTNAME}
networks:
  proxy:
    external: true
```

Open the configuration file "mailcow.conf" for editing.

```
nano /opt/containers/mailcow/mailcow.conf
```

Alter the values of the following fields to the values specified below.

```
# Skip running ACME (acme-mailcow, Let's Encrypt certs) - y/n
SKIP_LETS_ENCRYPT=y

# Skip ClamAV (clamd-mailcow) anti-virus (Rspamd will auto-detect a missing ClamAV container) - y/n
SKIP_CLAMD=y
```

Build and start the Docker containers and check the resulting log for any errors. Wait for half a minute or so before reviewing the logs, as it takes a little while for the containers to start.

```
docker-compose up -d
docker-compose logs
```

### More DNS Records

Enter the administration panel where you administrate your DNS records.

Create an rDNS record pointing to the fully qualified domain name of the e-mail server e.g. mail.example.com.

Create an MX record pointing mail.example.com to your valid registered domain name e.g. example.com.

To create an SPF record, create a TXT record linked to example.com, inserting the public IP address of your server, as shown below.

```
v=spf1 ip4:xxx.xxx.xxx.xxx -all 
```

To create a dmarc record, create a TXT record linked to _dmarc.example.com, in the format shown below.

```
v=DMARC1;p=reject;sp=none;adkim=r;aspf=r;pct=100;fo=0;rf=afrf;ri=86400 
```

To create a DKIM record, create a TXT record linked to dkim._domainkey.example.com, in the format shown below. The DKIM key can be located in the Mailcow configuration, accessible from your web browser at the address mail.example.com (see next section).

```
v=DKIM1;k=rsa;t=s;s=email;p=<insert key here>
```

Create an SRV record linked to _autodiscover._tcp.example.com with the fields populated in the format shown below.

```
Priority: 0
Weight: 0
Port: 443
Target: mail.example.com
```

You may wish to create the following CNAME records, linking the following subdomains to your valid registered domain name e.g. example.com.

* autodiscover.example.com
* autoconfig.example.com

### Verify Certificates

Entering the following commands into your server's terminal should produce sensible output.

```
# Connect via SMTP (587)
echo "Q" | openssl s_client -starttls smtp -crlf -connect mx.example.com:587

# Connect via IMAP (143)
echo "Q" | openssl s_client -starttls imap -showcerts -connect mx.example.com:143
```

### Configure Mailcow

**Logging In**

When logging in to mail.example.com for the first time, the username is "admin" and the password is "moohoo".

On the page which loads when you login, press the "Edit" button underneath the heading "Edit administrator details" to change the admin password.

**Add Domain**

Observe the dropdown menus at the very top of the page on the right side of the window. Press "Configuration" and choose "Mail Setup". Press the button "Add domain" and add your valid registered domain name e.g. example.com (do not enter the fully qualified domain name i.e. omit the "mail" prefix). Then press the button to save the change and restart SOGo.

**DKIM Key**

Observe the dropdown menus at the very top of the page on the right side of the window. Press "Configuration" and choose "Configuration and Details". Observe the row of dropdown menus a row down towards the left side of the window. Press "Configuration" and choose "ARC/DKIM keys". This is where you can add a DKIM key to copy over into your DKIM record in the DNS settings (see previous section).

**Test DNS Records**

Observe the dropdown menus at the very top of the page on the right side of the window. Press "Configuration" and choose "Mail Setup". Under the "Action" heading, press "DNS".

**Add Mailbox**

Observe the dropdown menus at the very top of the page on the right side of the window. Press "Configuration" and choose "Mail Setup". Observe the row of dropdown menus a row down towards the left side of the window. Choose "Mailboxes" and press "Add mailbox".

The user which you create in association with the mail box will be able to login at https://mail.example.com/SOGo.

### Testing

Various tests of your setup can be performed from the following websites.

* mail-tester.com
* mxtoolbox.com

### Maintenance

#### Performing a Manual Software Update

The setup of Mailcow employed in this tutorial makes use of the following Docker images.

|Image|
--- |
|humenius/traefik-certs-dumper|
|mailcow/unbound|
|mariadb|
|redis:6-alpine|
|mailcow/clamd|
|mailcow/rspamd|
|mailcow/phpfpm|
|mailcow/sogo|
|mailcow/dovecot|
|mailcow/postfix|
|nginx:mainline-alpine|
|memcached:alpine|
|mailcow/acme|
|mailcow/netfilter|
|mailcow/watchdog|
|mailcow/dockerapi|
|mailcow/solr|
|mailcow/olefy|
|mcuadros/ofelia|
|robbertkl/ipv6nat|

Join the https://t.me/mailcowupdates channel in Telegram to receive an alert when a new version of Mailcow becomes available. 

To perform a major update of your Mailcow server, necessary after receiving such an alert, run the update script which comes with Mailcow.

```
./update.sh
```

Updated containers are pushed to Docker Hub approximately daily. These generally just contain a routine `apt-get upgrade` rather than any changes to Mailcow itself. To perform a minor update, as is appropriate in this instance, run the following commands.

```
docker-compose pull
docker-compose down
docker-compose up -d
```

See the chapter [Automated Software Updates](chapter3-8.md) for instructions to automate the update process.

### Bonus

To benefit from in-browser handling of PGP encryption, you could integrate your Mailcow server with your own instance of the open source [Afterlogic WebMail Lite](https://afterlogic.org/webmail-lite) client. Instructions to do so are available here. https://autoize.ch/pgp-email-encryption-with-afterlogic-webmail-and-mailcow/

\- [start](README.md) - [previous](chapter3-3.md) - [next](chapter3-5.md) - [end](chapter4.md) -
