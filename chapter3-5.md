\- [start](README.md) - [previous](chapter3-4.md) - [next](chapter3-6.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 5: Self-Hosted Instant Messaging, Audio and Video Calls using Matrix

This section describes how to set up a Synapse server to enable communication with other users of the Matrix communication protocol and an optional Element web client. Before proceeding you may wish to compare Matrix and XMPP. If you do decide to enter the Matrix universe, you may wish to compare the Synapse and Dendrite server implementations before proceeding.

For the purpose of this guide, it will be assumed that you will host Matrix on a unique domain, for which we will use `example.net` as a placeholder. The address of a user of your service will then be in the format `user:example.net`. If you host other services under the same domain name, you could instead host Matrix on `matrix.example.net`, however this is not considered to be in line with best practise due to the theoretical possibility of XSS attacks.

If you will optionally host your own Element instance, it shall be assumed, in line with best practise, that you will do so on a different domain to that hosting the Matrix server e.g. `element.example.com`.

<figure>
<img src="images/element.png" alt="Element" style="width:90%;"/>
<figcaption>Fig. 3.5.1: A conversation held over the Matrix communication protocol in the Element client. Source: <a href="https://commons.wikimedia.org/wiki/File:Riot.im_Matrix_client_UI.png">WikiMedia Commons</a> (License: CC BY-SA 4.0).</figcaption>
</figure>

### Preparation

It is assumed that you have already performed basic system configuration as described in [section 2](chapter3-2.md) and that you have set up Traefik as described in [section 3](chapter3-3.md).

Start an interactive sudo session and change into the directory where you keep your docker containers.

```
sudo -i
cd /opt/containers
```

Create the following directories.

```
mkdir -p matrix/data/{nginx,synapse,postgres}
```

Create a 'docker-compose.yaml' configuration file and open it for writing.

```
cd matrix
touch docker-compose.yaml
nano docker-compose.yaml
```

Populate it with the following content. Here "**proxy**" is the name of the docker network which you created when you set up [Traefik](chapter3-3.md).

```
version: '2'

services:

# Configuration for Postgres

# Configuration for Nginx

# Configuration for Synapse

networks:
  matrix:
  proxy:
    external: true
```

### Configure DNS Records

Enter the administration panel where you administrate your DNS records. It is assumed that you already have an A record which maps a valid domain name to the public IP address of your server.

You must create CNAME records which map the following subdomains to your domain name e.g. example.net.

* synapse.example.net
* element.example.com (optional)

### Install an Element Web Client
***This step is optional***.

You will probably be better off using a native client on your client device. If however you want to set up your own web client, here is how.

**Warning**

> We do not recommend running Element from the same domain name as your Matrix homeserver. The reason is the risk of XSS (cross-site-scripting) vulnerabilities that could occur if someone caused Element to load and render malicious user generated content from a Matrix API which then had trusted access to Element (or other apps) due to sharing the same domain.<br><br>
We have put some coarse mitigations into place to try to protect against this situation, but it's still not good practice to do it in the first place. See https://github.com/vector-im/element-web/issues/1977 for more details.

*Source:* https://github.com/vector-im/element-web/

<br>Create the following directory and configuration file and open it for editing.

```
mkdir data/element
touch data/element/config.json
nano data/element/config.json
```

Here is a sample of possible content to put inside 'config.json'. Note that you may need to delete the comments for the configuration file to be valid.

```
{
  "default_server_config": {
    "m.homeserver": {  # REPLACE BOTH ENTRIES BELOW WITH A VALID DOMAIN NAME
      "base_url": "https://example.net",
      "server_name": "example.net"
    },
    "m.identity_server": {
      "base_url": "https://vector.im"
    }
  },
  "disable_custom_urls": true,
  "disable_guests": false,
  "disable_login_language_selector": false,
  "disable_3pid_login": false,
  "brand": "Element",
  "integrations_ui_url": "https://scalar.vector.im/",
  "integrations_rest_url": "https://scalar.vector.im/api",
  "integrations_widgets_urls": [
    "https://scalar.vector.im/_matrix/integrations/v1",
    "https://scalar.vector.im/api",
    "https://scalar-staging.vector.im/_matrix/integrations/v1",
    "https://scalar-staging.vector.im/api",
    "https://scalar-staging.riot.im/scalar/api"
  ],
  "bug_report_endpoint_url": "https://riot.im/bugreports/submit",
  "defaultCountryCode": "GB",
  "showLabsSettings": false,
  "features": {
    "feature_new_spinner": "labs",
    "feature_pinning": "labs",
    "feature_custom_status": "labs",
    "feature_custom_tags": "labs",
    "feature_state_counters": "labs"
  },
  "default_federate": true,
  "default_theme": "light",
  "roomDirectory": {
    "servers": [
      "matrix.org"
    ]
  },
  "welcomeUserId": "@riot-bot:matrix.org",
  "piwik": false,
  "enable_presence_by_hs_url": {
    "https://matrix.org": false,
    "https://matrix-client.matrix.org": false
  },
  "settingDefaults": {
    "breadcrumbs": true
  },
  "jitsi": {
    "preferredDomain": "jitsi.riot.im"
  }
}
```

Create a 'docker-compose.yaml' configuration file and open it for writing.

```
mkdir element
touch element/docker-compose.yaml
nano element/docker-compose.yaml
```

Insert the following content in 'docker-compose.yaml'. 

```
version: '2'

services:
  element:
    container_name: "element-matrix"
    image: "vectorim/element-web:latest"
    volumes:
      - "/opt/containers/matrix/data/element/config.json:/app/config.json:ro"  # ADJUST PATH TO REFLECT YOUR DIRECTORY STRUCTURE
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.element-secure.entrypoints=https"
      - "traefik.http.routers.element-secure.rule=Host(`element.example.com`)"  # REPLACE WITH VALID SUBDOMAIN NAME
      - "traefik.http.routers.element-secure.tls=true"
      - "traefik.http.routers.element-secure.tls.certresolver=http"
      - "traefik.http.routers.element-secure.middlewares=secHeaders@file"
      - "traefik.http.routers.element-secure.service=element"
      - "traefik.http.services.element.loadbalancer.server.port=80"
      - "traefik.docker.network=proxy"
    networks:
      - proxy

networks:
  proxy:
    external: true
```

Now build the docker container and after waiting for a few additional seconds, check the resulting log for errors.

```
cd element
docker-compose up -d
docker-compose logs
cd ..
```

Enter https://element.example.com in your web browser. The Element web client should load successfully.

### Install the Postgres Database

Synapse allows for the automatic creation of an SQLite database for data storage but due to limited concurrency this is suitable for use in testing only. Using Postgres is recommended for any serious deployment.

Insert the following content in 'docker-compose.yaml'. 

```
# Configuration for Postgres

  postgres:
    container_name: "postgres-matrix"
    image: "postgres:14.2"
    restart: "unless-stopped"
    environment:
      POSTGRES_DB: dummy
      POSTGRES_USER: admin
      POSTGRES_PASSWORD: insert_password_here  # SET A PASSWORD HERE
    volumes:
      - "/opt/containers/matrix/data/postgres:/var/lib/postgresql/data"  # ADJUST PATH TO REFLECT YOUR DIRECTORY STRUCTURE
    labels:
      - "traefik.enable=false"
    networks:
      - matrix
```

Build the Docker container and after waiting for a few additional seconds, verify the resulting log for any errors.

```
docker-compose up -d
docker-compose logs
```

Enter the Docker container and create a database using the commands shown below.

```
docker exec -it postgres-matrix psql dummy -U admin

CREATE ROLE synapse;
ALTER ROLE synapse WITH PASSWORD 'insert_password_here';  # SET A PASSWORD HERE
ALTER ROLE synapse WITH LOGIN;
CREATE DATABASE synapse ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER synapse;
GRANT ALL PRIVILEGES ON DATABASE synapse TO synapse;

\q
```

### Install Synapse

**Warning**

> Matrix serves raw, user-supplied data in some APIs -- specifically the content repository endpoints.<br><br>
Whilst we make a reasonable effort to mitigate against XSS attacks (for instance, by using CSP), a Matrix homeserver should not be hosted on a domain hosting other web applications. This especially applies to sharing the domain with Matrix web clients and other sensitive applications like webmail. See https://developer.github.com/changes/2014-04-25-user-content-security for more information.<br><br>
Ideally, the homeserver should not simply be on a different subdomain, but on a completely different registered domain (also known as top-level site or eTLD+1). This is because some attacks are still possible as long as the two applications share the same registered domain.<br><br>
To illustrate this with an example, if your Element Web or other sensitive web application is hosted on A.example1.com, you should ideally host Synapse on example2.com. Some amount of protection is offered by hosting on B.example1.com instead, so this is also acceptable in some scenarios. However, you should not host your Synapse on A.example1.com.<br><br>
Note that all of the above refers exclusively to the domain used in Synapse's public_baseurl setting. In particular, it has no bearing on the domain mentioned in MXIDs hosted on that server.<br><br>
Following this advice ensures that even if an XSS is found in Synapse, the impact to other applications will be minimal.

*Source:* https://github.com/matrix-org/synapse#security-note

<br>Create a sample Synapse config by running the following command.

```
# UPDATE THE PATH /opt/containers/matrix/data/synapse TO REFLECT YOUR DIRECTORY STRUCTURE
# UPDATE SYNAPSE_SERVER_NAME WITH VALID DOMAIN NAME

docker run -it --rm \
    -v /opt/containers/matrix/data/synapse:/data \
    -e SYNAPSE_SERVER_NAME=example.net \
    -e SYNAPSE_REPORT_STATS=yes \
    -e UID=1000 \
    -e GID=1000 \
    matrixdotorg/synapse:latest generate
```

Preserve a copy of the sample 'homeserver.yaml' configuration file before creating a new one and opening the new file for editing.

```
mv data/synapse/homeserver.yaml data/synapse/homeserver.yaml.bak
touch data/synapse/homeserver.yaml
nano data/synapse/homeserver.yaml
```

This sample configuration may be used to populate 'homeserver.yaml'.

```
server_name: "example.net"  # UPDATE WITH VALID DOMAIN NAME

pid_file: /data/homeserver.pid

web_client_location: https://element.example.net/  # UPDATE WITH VALID SUBDOMAIN NAME

public_baseurl: https://synapse.example.net/  # UPDATE WITH VALID SUBDOMAIN NAME

report_stats: true

enable_registration: true

admin_contact: 'mailto:example@example.com' # UPDATE WITH A VALID E-MAIL ADDRESS

listeners:
  - port: 8008
    tls: false
    type: http
    x_forwarded: true

    resources:
      - names: [client, federation]
        compress: false

retention:
  enabled: true

federation_ip_range_blacklist:
  - '127.0.0.0/8'
  - '10.0.0.0/8'
  - '172.16.0.0/12'
  - '192.168.0.0/16'
  - '100.64.0.0/10'
  - '169.254.0.0/16'
  - '::1/128'
  - 'fe80::/64'
  - 'fc00::/7'

database:
  name: psycopg2
  args:
    user: synapse
    password: insert_password_here  # SET IN ACCORDANCE WITH POSTGRESDB PASSWORD SET EARLIER 
    database: synapse
    host: postgres   
    cp_min: 5
    cp_max: 10

log_config: "/data/example.net.log.config"  # UPDATE WITH A VALID DOMAIN NAME

media_store_path: "/data/media_store"

macaroon_secret_key: "insert_password_here"  # MAKE UP A STRONG PASSWORD HERE

form_secret: "insert_password_here"  # MAKE UP A STRONG PASSWORD HERE

signing_key_path: "/data/example.net.signing.key"  # UPDATE WITH A VALID DOMAIN NAME

trusted_key_servers:
  - server_name: "matrix.org"

redis:
  enabled: false
```

Insert the following content in 'docker-compose.yaml'. 

```
# Configuration for Synapse

  synapse:
    container_name: "synapse-matrix"
    image: "matrixdotorg/synapse:latest"
    restart: "unless-stopped"
    environment:
      SYNAPSE_CONFIG_DIR: "/data/"
      SYNAPSE_CONFIG_PATH: "/data/homeserver.yaml"
      UID: "1000"
      GID: "1000"
      TZ: "Europe/London"  # SET TO THE TIMEZONE OF YOUR SERVER
    volumes:
      - "/opt/containers/matrix/data/synapse/:/data"  # ADJUST PATH TO REFLECT YOUR DIRECTORY STRUCTURE
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.synapse-secure.entrypoints=https"
      - "traefik.http.routers.synapse-secure.rule=Host(`synapse.example.net`)"  # UPDATE WITH VALID SUBDOMAIN
      - "traefik.http.routers.synapse-secure.tls=true"
      - "traefik.http.routers.synapse-secure.tls.certresolver=http"
      - "traefik.http.routers.synapse-secure.middlewares=secHeaders@file"
      - "traefik.http.routers.synapse-secure.service=synapse"
      - "traefik.http.services.synapse.loadbalancer.server.port=8008"
      - "traefik.docker.network=proxy"
    networks:
      - matrix
      - proxy
```

### Install Nginx

Finally set up an Nginx reverse proxy.

Create the following configuration file and open it for editing.

```
touch matrix/data/nginx/matrix.conf
nano matrix/data/nginx/matrix.conf
```

Populate the file "matrix.conf" with the following content.

```
# UPDATE TO A VALID DOMAIN NAME
server {
  listen         80 default_server;
  server_name    example.net;

 # Traefik -> nginx -> synapse
 location /_matrix {
    proxy_pass http://synapse:8008;
    proxy_set_header X-Forwarded-For $remote_addr;
    client_max_body_size 128m;
  }

  location /.well-known/matrix/ {
    root /var/www/;
    default_type application/json;
    add_header Access-Control-Allow-Origin  *;
  }
}
```

Create the following directories and a configuration file and open the file for editing.

```
mkdir -p matrix/data/nginx/www/.well-known/matrix
touch matrix/data/nginx/www/.well-known/matrix/client
nano matrix/data/nginx/www/.well-known/matrix/client
```

Populate the file with the following content.

**Note that you must delete the comment for the configuration file to be valid.**

```
{
  "m.homeserver": {
    "base_url": "https://example.net"  # UPDATE WITH A VALID DOMAIN NAME
  }
}
```

Create the following file and open it for editing.

```
touch matrix/data/nginx/www/.well-known/matrix/server
nano matrix/data/nginx/www/.well-known/matrix/server
```

Populate the file with the following content.

**Note again that you must delete the comment for the configuration file to be valid.**

```
{
  "m.server": "synapse.example.net:443"  # UPDATE WITH A VALID SUBDOMAIN
}
```

Insert the following content in 'docker-compose.yaml'.

```
# Configuration for Nginx

  nginx-matrix:
    container_name: "nginx-matrix"
    image: "nginx:latest"
    restart: "unless-stopped"
    volumes:
      - "/opt/containers/matrix/data/nginx/matrix.conf:/etc/nginx/conf.d/matrix.conf"  # ADJUST PATH TO REFLECT YOUR DIRECTORY STRUCTURE
      - /opt/containers/matrix/data/nginx/www:/var/www/  # ADJUST PATH TO REFLECT YOUR DIRECTORY STRUCTURE
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.nginx-matrix-secure.entrypoints=https"
      - "traefik.http.routers.nginx-matrix-secure.rule=Host(`example.net`)"  # UPDATE TO A VALID DOMAIN NAME
      - "traefik.http.routers.nginx-matrix-secure.tls=true"
      - "traefik.http.routers.nginx-matrix-secure.tls.certresolver=http"
      - "traefik.http.routers.nginx-matrix-secure.middlewares=secHeaders@file"
      - "traefik.http.routers.nginx-matrix-secure.service=nginx-matrix"
      - "traefik.http.services.nginx-matrix.loadbalancer.server.port=80"
      - "traefik.docker.network=proxy"
    networks:
      - proxy
```

Build the Docker container and after waiting for a few additional seconds, verify the resulting log for errors.

```
docker-compose up -d
docker-compose logs
```

Verify that the following URLs display the expected content, as described below.

https://example.net/.well-known/matrix/client displays `"base_url": "https://example.net"`.<br>
https://example.net/.well-known/matrix/server displays `"m.server": "synapse.example.net:443"`.<br>
https://example.net/_matrix/static/ and https://synapse.example.net/_matrix/static/ display the Synapse homepage.

You can verify your setup at https://federationtester.matrix.org/ by entering example.net (without https://).

Assuming that there were no errors, you should now be able to connect to your Matrix server using any client.

### Install a TURN Server

It is generally necessary to set up a TURN server in order for voice calls to function reliably. To do so, follow these instructions.

Create a directory in which to store the Docker configuration for the TURN server.

```
mkdir /opt/containers/matrix/coturn
cd /opt/containers/matrix/coturn
touch docker-compose.yaml
touch turnserver.conf
```

Populate `turnserver.conf` with the following content. Remove the comments before saving the file.

```
use-auth-secret
static-auth-secret=create_password_here # MAKE UP A STRONG PASSWORD HERE
realm=example.net # UPDATE WITH A VALID DOMAIN NAME
listening-port=3478
tls-listening-port=5349
min-port=49160
max-port=49200
verbose
allow-loopback-peers
cli-password=create_password_here # MAKE UP A STRONG PASSWORD HERE
external-ip=xxx.xxx.xxx.xxx # REPLACE WITH YOUR SERVER'S PUBLIC IP ADDRESS
```

Populate `docker-compose.yaml` with the following content.

```
version: '3'

services:
  coturn:
    container_name: "coturn"
    image: "coturn/coturn:latest"
    restart: "unless-stopped"
    volumes: # ENSURE THAT THE PATH OF THE LEFT HAND SIDE IS VALID
      - "/opt/containers/matrix/coturn/turnserver.conf:/etc/turnserver.conf"
    ports:
      - "49160-49200:49160-49200/udp"
      - "3478:3478"
      - "5349:5349"
```

Now build the docker container and after waiting for a few additional seconds, check the resulting log for errors.

```
docker-compose up -d
docker-compose logs
```

Note that the following error message can apparently simply be ignored.

```
ERROR: Could not start Prometheus collector!
```

Next it is necessary to configure Synapse to use the TURN server.

```
nano /opt/containers/matrix/data/synapse/homeserver.yaml
```

Insert the following content into the configuration file. Remove the comments before saving the file.

```
turn_uris:
  - "turn:example.net:3478?transport=udp" # UPDATE WITH VALID DOMAIN NAME
  - "turn:example.net:3478?transport=tcp" # UPDATE WITH VALID DOMAIN NAME
  - "turns:example.net:3478?transport=udp" # UPDATE WITH VALID DOMAIN NAME
  - "turns:example.net:3478?transport=tcp" # UPDATE WITH VALID DOMAIN NAME
turn_shared_secret: insert_password_here # INSERT STATIC-AUTH-SECRET DEFINED EARLIER
turn_user_lifetime: 86400000
turn_allow_guests: True
```

Restart the Matrix containers. Wait for a few seconds before reviewing the resulting log for errors.

```
cd /opt/containers/matrix
docker-compose down
docker-compose up -d
docker-compose logs
```

### Administrate Users

The configuration defined above allows users to sign up to your Matrix homeserver from their client of choice.

If you wish to prevent just anyone from being able to create a user, you can instead create an admin user who will have the power to create user accounts using the Matrix API.

First disable user initiated signups. Open `data/synapse/homeserver.yaml` for editing.

```
nano data/synapse/homeserver.yaml
```

Change the value of the `enable_registration` property to `false` and create the `registration_shared_secret` property, thinking up a strong password.

```
enable_registration: false

registration_shared_secret: insert_password_here
```

Rebuild the Docker container.

```
docker-compose down
docker-compose up -d
```

If you have previously created a user using a Matrix client, then to grant this user admin rights, login to the container which is running postgres and set the user's 'admin' attribute to true.

```
docker exec -it postgres-matrix psql synapse -U synapse

# REPLACE WITH A VALID DOMAIN NAME
UPDATE users SET admin = 1 WHERE name = '@username:example.net';
```

If you have not yet created a user, you can do so by logging into the container which is running synapse and executing the `register_new_matrix_user` command. You will be prompted to specify a username, password and whether the user is to be granted admin rights.

```
docker exec -it synapse-matrix sh

# REPLACE WITH A VALID DOMAIN NAME
register_new_matrix_user -c /data/homeserver.yaml https://synapse.example.net
```

To work with the API, you must first retrieve the admin user's access token. Execute the following command from any command line terminal.

```
curl -XPOST -d \
    '{"type":"m.login.password", "user":"insert_username_here", "password":"insert_password_here"}' \
    "https://synapse.example.net/_matrix/client/r0/login"
```

Take note of the value of the `access_token` property which is returned in response to this query.

You can now accomplish the following actions by executing the specified command from any command line terminal.

**List all users:**

```
curl --header "Authorization: Bearer insert_access_token" -XGET \
    https://synapse.example.net/_synapse/admin/v2/users?from=0&limit=10&guests=false
```

**Query a user:**

```
curl --header "Authorization: Bearer insert_access_token" -XGET \
    https://synapse.example.net/_synapse/admin/v2/users/@username:example.net
```

**Create or modify a user:**

```
curl --header "Authorization: Bearer insert_access_token" -XPUT -d \
    '{"displayname":"insert_display_name", "password": "insert_password"}' \
    https://synapse.example.net/_synapse/admin/v2/users/@username:example.net
```

**Deactivate a user:**

```
curl --header "Authorization: Bearer insert_access_token" -XPOST -d \
    '{"erase": true}' \
    https://synapse.example.net/_synapse/admin/v1/deactivate/@username:example.net
```

### Maintenance

#### Performing a Manual Software Update

Keeping your Synapse (Matrix) instance up to date entails pulling the latest images listed in the table below for the corresponding tags.

|Container|Tag|
--- | --- |
|matrixdotorg/synapse|latest|
|postgres|14.2|
|nginx|latest|
|coturn/coturn|latest|
|vectorim/riot-web|latest|

Release announcements for Element and Synapse are posted in the following Matrix groups.

- Element Web Announcements.
- Synapse Announcements.

To perform a manual update, pulling the latest container image for each tag and rebuilding any out of date containers, execute the following commands.

```
docker-compose pull
docker-compose up -d --remove-orphans

# Optionally remove obsolete images
docker image prune
```

See the chapter [Automated Software Updates](chapter3-8.md) for instructions about automating the update process.

#### Verify Version Info

To verify the version of Synapse which has been deployed, execute the following query from any command line terminal.

```
curl --header "Authorization: Bearer insert_access_token" -XGET \
    https://synapse.example.net/_synapse/admin/v1/server_version
```

To verify the version of Element which has been deployed, enter the `Help & About` menu under `All settings`.

\- [start](README.md) - [previous](chapter3-4.md) - [next](chapter3-6.md) - [end](chapter4.md) -
