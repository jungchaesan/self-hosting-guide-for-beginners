\- [start](README.md) - [previous](chapter3-7.md) - [next](chapter4.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 8: Automated Software Ugrades

To maintain good security, it is essential that you keep the software on your homeserver up-to-date. This section details how to set up automatic software upgrades.

### Automatic Software Upgrades on Debian

Here we describe how to set up the automatic upgrade of packages from the core Debian repositories.

Start an interactive sudo session. Install the `unattended-upgrades` package if it is not already installed.

```
sudo -i
apt-get update
apt-get install unattended-upgrades
```

Open the `/etc/apt/apt.conf.d/50unattended-upgrades` file for editing. Uncomment the following lines.

```
"origin=Debian,codename=${distro_codename}-updates";
"origin=Debian,codename=${distro_codename}-proposed-updates";
"origin=Debian,codename=${distro_codename},label=Debian";
"origin=Debian,codename=${distro_codename},label=Debian-Security";
```

Also add the following line directly beneath the 4 lines specified above so that Docker packages will be automatically upgraded.

```
"origin=Docker,codename=${distro_codename}";
```

Scroll further down and uncomment the following line to specify an e-mail address should you wish to be informed after upgrades have been performed.

```
Unattended-Upgrade::Mail "example@example.com";
```

Scroll further down and uncomment the following line to automate the removal of unused dependencies.

```
Unattended-Upgrade::Remove-Unused-Dependencies "true";
```

Run the following command to enable automatic upgrades.

```
dpkg-reconfigure --priority=low unattended-upgrades
```

You can run the following command to verify that that unattended upgrade service is running.

```
systemctl status unattended-upgrades.service
```

Upgrades are logged in the `/var/log/unattended-upgrades/` directory.

### Automatic Upgrades of Docker Containers

This section describes how to set up automatic upgrades for services which you have set up using Docker Compose. Note that any services requiring manual intervention or a more complex procedure to upgrade must be dealt with separately. You may get away with upgrading Jitsi in accordance with these instructions, however as Jitsi does not maintain backwards compatibility for configuration files, be aware that it might break after an upgrade, requiring manual rollback or repair. Traefik, Nextcloud and Synapse (Matrix) can be upgraded using the procedure described here. Mailcow is treated in a separate sub-section.

#### Prequisites

The update scripts used in the following sections send an e-mail report on completion of an upgrade using the `msmtp` utility.

Start an interactive sudo session and install the `msmtp` package.

```
sudo -i
apt-get update
apt-get install msmtp
```

Create the following configuration file and open it for editing.

```
touch ~/.msmtprc
nano ~/.msmtprc
```

Populate the configuration file with the following contents. You may need to delete the comments for the configuration file to be valid.

```
defaults
tls on

account example  # UPDATE WITH THE DOMAIN OF YOUR E-MAIL SERVER
auth on
host mail.example.com  # UPDATE WITH THE VALID SUBDOMAIN OF YOUR E-MAIL SERVER
port 587
user example@example.com  # UPDATE WITH THE USERNAME OF A VALID E_MAIL ACCOUNT
password insert_password_here  # UPDATE WITH THE PASSWORD OF A VALID E-MAIL ACCOUNT
from example@example.com  # UPDATE WITH THE USERNAME OF A VALID E-MAIL ACCOUNT

account default : example  # UPDATE WITH THE DOMAIN OF YOUR E-MAIL SERVER
```

You should now be able to send e-mail from the command line using the `msmtp` uility.

#### Update Scripts

Start an interactive sudo session. Create a new directory called `update` in the location where you keep your Docker containers. Place the update scripts located in the `scripts` directory of this repository there.

```
sudo -i
mkdir /opt/containers/update
```

Open `/opt/containers/update/apply_updates.sh` for editing. Verify that the path specified in the variable `UPDATE_DIR` reflects your directory structure.

```
readonly UPDATE_DIR='/opt/containers/update' # VERIFY THAT PATH REFLECTS YOUR DIRECTORY STRUCTURE
```

Open `/opt/containers/update/update_app.sh` for editing. Update the following line with a valid e-mail address.

```
# UPDATE WITH A VALID E-MAIL ADDRESS 
echo | (echo "Subject: Software Update Applied to Docker Container(s)" ; cat $TEMP_LOG_FILE) 
| msmtp example@example.com
```

Open the `/opt/containers/update/update_list.conf` file for editing. For each service for which you want to implement automatic updates, insert a line containing the directory from which you launched the service with Docker Compose.

```
/opt/containers/traefik
/opt/containers/nextcloud
/opt/containers/matrix
/opt/containers/jitsi
/opt/containers/matrix/coturn
```

If you decide to include Jitsi, verify that the tags of all Docker images in `/opt/containers/jitsi/docker-compose.yml` are set to `latest`.

Now make the update scripts executable.

```
chmod a+x /opt/containers/update/*.sh
```

To create a system crontab entry which executes the update script at a time of your choosing, open the `/etc/crontab` file for editing. Appending the following line would schedule an update at 4 am.

```
0 4 * * * root /opt/containers/update/apply_updates.sh
```

A timestamp is saved in `/opt/containers/update/apply_updates.log` when the update script is executed.

Information about which containers have been updated for an application is saved in the `update.log` file, saved to the home directory of the application e.g. `/opt/containers/matrix/update.log`.

#### Mailcow Update Script

A separate script `update_mailcow.sh` has been provided for updating Mailcow. Open it for editing.

```
nano /opt/containers/update/update_mailcow.sh
```

Verify that the path specified in the variable `MAILCOW_DIR` reflects your directory structure.

```
readonly MAILCOW_DIR='/opt/containers/mailcow' # VERIFY THAT PATH REFLECTS YOUR DIRECTORY STRUCTURE
```

Update the following line with a valid e-mail address.

```
# UPDATE WITH A VALID E-MAIL ADDRESS
echo | (echo "Subject: Software Update Applied to Mailcow" ; cat $TEMP_LOG_FILE) 
| msmtp example@example.com
```

Next open the `/etc/crontab` file for editing. Appending the following line would schedule an update at 4:20 am.

```
20 4 * * * root /opt/containers/update/update_mailcow.sh
```

\- [start](README.md) - [previous](chapter3-7.md) - [next](chapter4.md) - [end](chapter4.md) -
