\- [start](README.md) - [previous](chapter2-1.md) - [next](chapter3-1.md) - [end](chapter4.md) -

# Chapter 2: Self-Hosting - Getting Started

## Section 2: Principle Components

This section describes some basic, fundamental components of a self-hosted setup.

### Hardware

The resource requirements (RAM, processing power and disk space) for the software which is intended to be self-hosted on the server are the principle factor motivating the choice of hardware.

Security aspects pertaining to the hardware in question and whether all of the constituent components may be run using open source drivers are also matters which merit consideration. The jurisdiction in which the hardware and its component parts are manufactured may also be relevant.

As an alternative to purchasing a single, powerful machine, it is possible to create a cluster pooling the resources from multiple computers by utilising distributed computing software such as Kubernetes.

An important consideration when choosing hardware is the processor architecture deployed in the hardware in question. Two principle types of processor architecture are currently widespread.

* The **ARM64** processor architecture (also known as AArch64) is utilised in most mobile phones and portable devices, as well as some single-board computers such as the Raspberry Pi. ARM64 processors consume less power, making them especially suitable for battery powered devices.
* The **x86-64** processor architecture (also known as AMD64) occurs in most laptops, desktop computers and server hardware currently sold on the market.

Software must be compiled separately for each processor architecture. A disadvantage associated with ARM64 is that some software packages are not routinely compiled and released for this processor architecture. Advantages applicable to ARM64 devices include low energy consumption, less heat and noise and lower price. One of the cheapest devices available on the market which is viable for use as a homeserver is the Raspberry Pi, utilising the ARM64 instruction set.

### Operating System

The base software running on any computer is called the operating system. The Free Software Foundation maintains a list of [*free* ("free as in freedom") operating systems](https://www.gnu.org/distros/free-distros.html). Most widely used GNU/Linux distributions for servers, laptops and desktop computers alike do not appear on the list because although generally free and open source, nevertheless in an effort to support a large variety of hardware, the installation of non-free device drivers is facilitated when required in order to make the computer work.

Debian is a popular choice of operating system for use on servers. Raspberry Pi OS is a derivative of Debian optimised for the Raspberry Pi. Some homeserver enthusiasts report good results with Manjaro.

### Container Engine

Utilising containers allows for a software service to be run inside an isolated environment, partitioned from the host computer and any other services running on the host. Containers are a realisation of the concept of OS-level virtualisation, whereby each container runs its own user space instance of an operating system of choice, while all containers running on the host system share the host's kernel. Containerised services interact with resources such as the host's file system and services running in other containers through carefully defined rules. Containers impart a number of advantages.

* Security: A security vulnerability in a containerised software service need not necessarily impact the wider system.
* Modularity: As each software service is ring-fenced, there is little scope for one containerised service to disrupt another. By way of illustration, instead of sharing a single database on the host, each containerised service typically utilises its own database, eliminating the possibility of cross interference.
* Portability: Porting a containerised setup onto new hardware may be as simple as zipping the container, copying it over and then unzipping and starting the container.

Popular container engines facilitating the use of containers include Docker and Podman. Docker is particularly widely used and well supported, however the Docker daemon runs containers as root, which in some circumstances may constitute a security risk. Podman on the other hand does not require that containers are run as root.

### Integrated Solutions

A recent development in home computing is the appearance of a number of integrated solutions facilitating the realisation of a homeserver setup. These integrated solutions typically bundle software services, security features, a container engine and operating system and in some cases even hardware.

|Solution Name|Uses Containers|Licence|Ships with Hardware|
|---|---|---|---|
|Mistborn|Yes|MIT License|No|
|FreedomBox|No|GNU Affero General Public License|Yes|
|Umbrel|Yes|PolyForm Noncommercial License|No|

All of the integrated solutions listed above support hardware utilising the ARM64 and x86-64 instruction sets.

Mistborn may be installed onto a server running a Debian based operating system. It uses Docker as the container engine. The project is released to the public under the fully permissive MIT License.

FreedomBox is built on Debian. It does not use containers. The project is released to the public under the free software GNU Affero General Public License. Small and very affordable homeservers may be ordered which ship with FreedomBox preinstalled, however due to the limited resources of the hardware in question, a better option may be to install it onto your own choice of hardware, should you intend to run resource intensive services.

Umbrel uses Docker as the container engine. The project is released under the PolyForm Noncommercial License and is thereby open source but not fully free in the sense that extending or adapting it and deriving revenues from your derived creation is prohibited. It used to be possible to order a device shipping with Umbrel preinstalled, however this is not currently the case at the time of writing.

### Resources

|URL|Description|
| --- | --- |
| https://www.pine64.org | A manufacturer of hardware for electronics and free software enthusiasts. |
| https://puri.sm | A US based hardware and software company with a strong free software ethos. |
| https://www.gnu.org/distros/free-distros.html | The Free Software Foundation's list of endorsed operating systems. |
| https://gitlab.com/cyber5k/mistborn | A free and open source homeserver solution integrating a number of advanced security features. |
| https://freedombox.org | An integrated free and open source homeserver solution. |
| https://umbrel.com | An integrated open source homeserver solution which at the time of writing is under development. |

\- [start](README.md) - [previous](chapter2-1.md) - [next](chapter3-1.md) - [end](chapter4.md) -
