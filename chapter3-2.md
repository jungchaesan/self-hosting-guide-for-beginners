\- [start](README.md) - [previous](chapter3-1.md) - [next](chapter3-3.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 2: Install Operating System and Container Engine

As a starting point it is necessary to install an operating system onto the server and to install Docker Compose. As an operating system we have chosen Debian.

### 1. Install Debian

At the time of writing, Debian 11 is the current stable release of Debian, released on the 14th of August 2021 and standing to benefit from official maintenance for five years from that date. The instructions in this guide were tested using **Debian 10**, released on the 6th of July 2019 and similarly standing to benefit from official support for 5 years from the date of release. 

To get started, install either Debian 10 or 11 onto your server and perform basic configuration such as setting up sudo and ssh.

### 2. Install Docker Compose

Login to your server locally or remotely e.g. using SSH. Start an interactive sudo session.

```
sudo -i
```

Carefully review the [Docker installation instructions](https://docs.docker.com/engine/install/debian/#install-using-the-repository), which at the time of writing are as follows.

```
# install dependencies
apt-get update
apt-get install ca-certificates curl gnupg lsb-release

# install Docker
mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/debian/gpg | \
sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo "deb [arch=$(dpkg --print-architecture) \
signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/debian \
$(lsb_release -cs) stable" | \
sudo tee /etc/apt/sources.list.d/docker.list > \
/dev/null

apt-get update
apt-get install docker-ce docker-ce-cli containerd.io

# test Docker
docker run hello-world
docker version
```

The output of the command `docker run hello-world` should contain the message "Hello from Docker!"

The output of the command `docker version` should display the version information of Docker.

Next install Docker Compose.

```
# UPDATE THE VERSION NUMBER BELOW TO THE LATEST VERSION AVAILABLE HERE: https://github.com/docker/compose/releases
curl -L "https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# make the binary executable
chmod +x /usr/local/bin/docker-compose

# test Docker Compose
docker-compose version
```

The output of the command `docker-compose version` should display the version information of Docker Compose.

\- [start](README.md) - [previous](chapter3-1.md) - [next](chapter3-3.md) - [end](chapter4.md) -
