\- [start](README.md) - [previous](chapter3-5.md) - [next](chapter3-7.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 6: Self-Hosted Video Conferencing using Jitsi

This section describes how to set up a Jitsi server for video conferencing.

<table><tr><td>
<figure>
<p align="center">
<img src="images/jitsi.png" alt="Jitsi" width="75%"/>
</p>
</figure>
</td></tr></table>
<figcaption>Fig. 3.6.1: Illustrations of a video conference held with Jitsi on a laptop and a mobile phone. Source: <a href="https://github.com/jitsi">https://github.com/jitsi</a> (Apache License 2.0).</figcaption>

### DNS Records

Enter the administration panel where you administrate your DNS records. Create a CNAME record mapping your chosen subdomain name "jitsi.example.com" to a valid registered domain name e.g. "example.com", for which an A record pointing to your server's public IP address is assumed to exist already.

### Setting Up Jitsi

It is assumed that you have already performed basic system configuration as described in [section 2](chapter3-2.md) and that you have set up Traefik as described in [section 3](chapter3-3.md).

Start an interactive sudo session and change into the directory where you keep your docker containers.

```
sudo -i
cd /opt/containers
```

Download and extract the source for the latest version of Jitsi for Docker.

```
mkdir downloads
# UPDATE VERSION NUMBER WITH THE LATEST RELEASE AVAILABLE HERE: https://github.com/jitsi/docker-jitsi-meet/releases/
wget https://github.com/jitsi/docker-jitsi-meet/archive/refs/tags/stable-5963.tar.gz -O downloads/jitsi.tar.gz
tar -xvzf downloads/jitsi.tar.gz -C ./
mv docker-jitsi-meet-stable-5963 jitsi
```

Create some folders required by Jitsi.

```
rm -r ~/.jitsi-meet-cfg/
mkdir -p ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody,jicofo,jvb,jigasi,jibri}
```

Copy the default configuration before modifying it.

```
cd jitsi
cp env.example .env
nano .env
```

Inside ".env", modify the "PUBLIC_URL" field to contain a valid fully qualified domain name.

```
PUBLIC_URL=https://jitsi.example.com 
```

To ensure that only authenticated users can start a video conference using your Jitsi instance, uncomment the fields pertaining to authentication and ensure the values are as follows.

```
ENABLE_AUTH=1
ENABLE_GUESTS=1
AUTH_TYPE=internal
```

Generate secure passwords.

```
./gen-passwords.sh
```

Open the "docker-compose.yaml" configuration file for editing.

```
nano docker-compose.yml
```

Locate the "web" node and add a "labels" subnode with the following content.

```
        labels:
            - "traefik.enable=true"
            - "traefik.http.routers.web.entrypoints=http"
            - "traefik.http.routers.web.rule=Host(`jitsi.example.com`)"  # UPDATE WITH A VALID SUBDOMAIN NAME
            - "traefik.http.middlewares.web-https-redirect.redirectscheme.scheme=https"
            - "traefik.http.routers.web.middlewares=web-https-redirect"
            - "traefik.http.routers.web-secure.entrypoints=https"
            - "traefik.http.routers.web-secure.rule=Host(`jitsi.example.com`)"  # UPDATE WITH A VALID SUBDOMAIN NAME
            - "traefik.http.routers.web-secure.tls=true"
            - "traefik.http.routers.web-secure.tls.certresolver=http"
            - "traefik.http.routers.web-secure.service=web"
            - "traefik.http.routers.web-secure.middlewares=secHeaders@file"
            - "traefik.http.services.web.loadbalancer.server.port=80"
            - "traefik.docker.network=proxy"
```

Under the "web" node, alter the "networks" subnode to contain the following.

```
        networks:
            proxy:
            meet.jitsi:
                aliases:
                    - ${XMPP_DOMAIN}
```

Append the last two lines of the following to the very bottom of the "docker-compose.yaml" file, so that the final four lines look as follows.

```
networks:
    meet.jitsi:
    proxy:
      external: true
```

Comment out the following lines in the "docker-compose.yaml" file.

```
        #ports:
            #- '${HTTP_PORT}:80'
            #- '${HTTPS_PORT}:443'
        environment:
            #- ENABLE_LETSENCRYPT
.
.
            #- LETSENCRYPT_DOMAIN
            #- LETSENCRYPT_EMAIL
            #- LETSENCRYPT_USE_STAGING
```

Build the Docker container and after waiting for half a minute or so for the container to start, verify the resulting logs for any errors.

```
docker-compose up -d
docker-compose logs
```

Navigate to jitsi.example.com in your web browser. It should load the Jitsi home page.

Before you can create a meeting, you must create a user. Other meeting participants may then enter the meeting as guests.

### Manage Users

To create a user, run the following command.

```
docker-compose exec prosody /bin/bash
prosodyctl --config /config/prosody.cfg.lua register TheDesiredUsername meet.jitsi TheDesiredPassword
```

To delete a user, run the following command.

```
prosodyctl --config /config/prosody.cfg.lua unregister TheDesiredUsername meet.jitsi
```

To list all users, run the following command.

```
find /config/data/meet%2ejitsi/accounts -type f -exec basename {} .dat \;
```

### Maintenance

#### Performing a Manual Software Update

To make it possible to keep your Jitsi installation up to date, you may change the tags of all Docker images in `docker-compose.yml` to `latest`. Note that the update will only be successful if there are no breaking changes to the formatting of configuration files, in which case manual intervention will be required.

|Container|Tag|
--- | --- |
|jitsi/web|latest|
|jitsi/prosody|latest|
|jitsi/jicofo|latest|
|jitsi/jvb|latest|

To perform a manual update, pulling the latest container image for each tag and rebuilding any out of date containers, execute the following commands.

```
docker-compose pull
docker-compose up -d --remove-orphans

# Optionally remove obsolete images
docker image prune
```

See the chapter [Automated Software Updates](chapter3-8.md) for instructions about automating the update process.

### Bonus

If you would like to share login credentials between multiple applications, you can configure Jitsi to use LDAP authentication. See the [official Jitsi documentation](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker) on Github for further details.


\- [start](README.md) - [previous](chapter3-5.md) - [next](chapter3-7.md) - [end](chapter4.md) -
