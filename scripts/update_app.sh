#!/bin/bash
#
# Update application running in Docker from current directory if updates available.

readonly LOG_FILE='update.log'
readonly TEMP_LOG_FILE='update_temp.log'
readonly TEMP_IMAGE_FILE='image_list.txt'

touch $LOG_FILE
touch $TEMP_LOG_FILE

# obtain list of containers from "docker-compose images" command
containers=()
docker-compose images>$TEMP_IMAGE_FILE

counter=0
while IFS=" " read -r container remainder
do
  if [[ $counter -lt 2 ]]; then
    ((counter++))
    continue
  fi
  containers+=($container)
done < $TEMP_IMAGE_FILE

# check if any container requires updating
is_first_iteration=1
need_update=0
for container in "${containers[@]}"; do
  # retrieve image and hash of running container
  container_image="$(docker inspect --format "{{.Config.Image}}" --type container ${container})"
  running_image="$(docker inspect --format "{{.Image}}" --type container "${container}")"

  # pull latest version of container and get hash
  docker pull "${container_image}"
  latest_image="$(docker inspect --format "{{.Id}}" --type image "${container_image}")"

  if [[ "${running_image}" != "${latest_image}" ]]; then
    if (( $is_first_iteration == 1 )); then
      echo -e "\n\n"$(date -u) >> $TEMP_LOG_FILE
      is_first_iteration=0
    fi
    echo "The image ${container_image} requires updating." >> $TEMP_LOG_FILE
    need_update=1
  fi
done

if (( $need_update == 1 )); then
  docker-compose pull >> $TEMP_LOG_FILE 2>&1
  docker-compose down >> $TEMP_LOG_FILE 2>&1
  docker-compose up -d --remove-orphans >> $TEMP_LOG_FILE 2>&1
  yes | docker system prune >> $TEMP_LOG_FILE 2>&1

  # Save log and send e-mail report
  cat $TEMP_LOG_FILE >> $LOG_FILE

  # UPDATE WITH A VALID E-MAIL ADDRESS
  echo | (echo "Subject: Software Update Applied to Docker Container(s)" ; cat $TEMP_LOG_FILE) | msmtp example@example.com
fi

rm $TEMP_IMAGE_FILE
rm $TEMP_LOG_FILE
