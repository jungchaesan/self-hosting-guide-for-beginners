#!/bin/bash
#
# Check if updates are available and update Mailcow.

readonly MAILCOW_DIR='/opt/containers/mailcow' # VERIFY THAT PATH REFLECTS YOUR DIRECTORY STRUCTURE
readonly LOG_FILE='update.log'
readonly TEMP_LOG_FILE='update_temp.log'

cd $MAILCOW_DIR
touch $LOG_FILE
touch $TEMP_LOG_FILE

if ./update.sh --check >/dev/null ; then
  # Perform update
  echo -e "\n\n"$(date -u) >>$TEMP_LOG_FILE
  ./update.sh --force >>$TEMP_LOG_FILE 2>&1 ; ret=$? ; echo "exit code = $ret"
  if [ $ret -eq 2 ] ; then
    echo "Running $MAILCOW_DIR/update.sh again" >>$TEMP_LOG_FILE
    ./update.sh --force >>$TEMP_LOG_FILE 2>&1 ; ret=$? ; echo "exit code = $ret"
  fi
  ./update.sh --gc >>$TEMP_LOG_FILE 2>&1 ; echo "exit code = $?"

  # Save log and send e-mail report
  cat $TEMP_LOG_FILE >> $LOG_FILE
  sleep 30

  # UPDATE WITH A VALID E-MAIL ADDRESS
  echo | (echo "Subject: Software Update Applied to Mailcow" ; cat $TEMP_LOG_FILE) | msmtp example@example.com
fi

rm $TEMP_LOG_FILE
