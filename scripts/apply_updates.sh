#!/bin/bash
#
# Perform software updates for applications specified in update_list.conf

readonly UPDATE_DIR='/opt/containers/update' # VERIFY THAT PATH REFLECTS YOUR DIRECTORY STRUCTURE
readonly UPDATE_LIST="$UPDATE_DIR/update_list.conf"
readonly UPDATE_SCRIPT="$UPDATE_DIR/update_app.sh"
readonly LOG_FILE="$UPDATE_DIR/apply_updates.log"

touch $LOG_FILE

declare -a directories
let i=0
while IFS=$'\n' read -r dir; do
  directories[i]="${dir}"
  ((++i))
done < $UPDATE_LIST

echo "apply_updates.sh was executed on "$(date -u) >> $LOG_FILE

for container in "${directories[@]}"; do
  cd $container
  $UPDATE_SCRIPT
done
