\- [start](README.md) - [previous](chapter3-6.md) - [next](chapter3-8.md) - [end](chapter4.md) -

# Chapter 3: A Self-Hosted Setup using Docker and Traefik

## Section 7: Self-Hosted File Cloud using Nextcloud

This section describes how to set up a self-hosted file cloud using Nextcloud.

<table><tr><td>
<figure>
<p align="center">
<img src="images/nextcloud.png" alt="Nextcloud" width="100%"/>
</p>
</figure>
</td></tr></table>
<figcaption>Fig. 3.7.1: A screenshot of the Nextcloud user interface. Source: <a href="https://github.com/nextcloud/server">https://github.com/nextcloud/server</a> (License: AGPLv3).</figcaption>

### DNS Records

Enter the administration panel where you administrate your DNS records. Create a CNAME record mapping your chosen subdomain name "cloud.example.com" to a valid registered domain name e.g. "example.com", for which an A record pointing to your server's public IP address is assumed to exist already.

### Setting Up Nextcloud

It is assumed that you have already performed basic system configuration as described in [section 2](chapter3-2.md) and that you have set up Traefik as described in [section 3](chapter3-3.md).

Start an interactive sudo session, change into the directory where you keep your Docker configuration and create the following folders.

```
sudo -i
cd /opt/containers
mkdir -p /opt/containers/nextcloud/{database,app,data}
```

Create a Docker Compose file and open it for editing.

```
touch /opt/containers/nextcloud/docker-compose.yml
nano /opt/containers/nextcloud/docker-compose.yml
```

Insert the following contents, altering fields where specified and thinking up strong passwords.

```
version: '2'
services:
  nextcloud-db:
    image: mariadb
    container_name: nextcloud-db
    command: --transaction-isolation=READ-COMMITTED --log-bin=ROW
    command: --innodb_read_only_compressed=OFF
    restart: unless-stopped
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - /opt/containers/nextcloud/database:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=insert_password_here # ENTER AN SQL ROOT PASSWORD
      - MYSQL_PASSWORD=insert_password_here # ENTER AN SQL USER PASSWORD
      - MYSQL_DATABASE=nextcloud # ENTER AN SQL DATABASE NAME
      - MYSQL_USER=nextcloud # ENTER AN SQL USERNAME
      - MYSQL_INITDB_SKIP_TZINFO=1
    networks:
      - default
  nextcloud-redis:
    image: redis:alpine
    container_name: nextcloud-redis
    hostname: nextcloud-redis
    networks:
        - default
    restart: unless-stopped
    command: redis-server --requirepass insert_password_here # ENTER A REDIS PASSWORD
  nextcloud-app:
    image: nextcloud
    container_name: nextcloud-app
    restart: unless-stopped
    depends_on:
      - nextcloud-db
      - nextcloud-redis
    environment:
        REDIS_HOST: nextcloud-redis
        REDIS_HOST_PASSWORD: insert_password_here # ENTER SAME REDIS PASSWORD AS ABOVE
    volumes:
      - /opt/containers/nextcloud/app:/var/www/html
      - /opt/containers/nextcloud/data:/var/www/html/data
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.nextcloud-app.entrypoints=http"
      - "traefik.http.routers.nextcloud-app.rule=Host(`cloud.example.com`)" # INSERT FULLY QUALIFIED DOMAIN NAME HERE
      - "traefik.http.middlewares.nextcloud-app-https-redirect.redirectscheme.scheme=https"
      - "traefik.http.routers.nextcloud-app.middlewares=nextcloud-app-https-redirect"
      - "traefik.http.routers.nextcloud-app-secure.entrypoints=https"
      - "traefik.http.routers.nextcloud-app-secure.rule=Host(`cloud.example.com`)" # INSERT FULLY QUALIFIED DOMAIN NAME HERE
      - "traefik.http.routers.nextcloud-app-secure.tls=true"
      - "traefik.http.routers.nextcloud-app-secure.tls.certresolver=http"
      - "traefik.http.routers.nextcloud-app-secure.service=nextcloud-app"
      - "traefik.http.services.nextcloud-app.loadbalancer.server.port=80"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.nextcloud-app-secure.middlewares=nextcloud-dav,secHeaders@file"
      - "traefik.http.middlewares.nextcloud-dav.replacepathregex.regex=^/.well-known/ca(l|rd)dav"
      - "traefik.http.middlewares.nextcloud-dav.replacepathregex.replacement=/remote.php/dav/"
    networks:
      - proxy
      - default
networks:
  proxy:
    external: true
```

Build the Docker container and after allowing half a minute or so for the container to start, verify the resulting logs.

```
cd nextcloud
docker-compose up -d
docker-compose logs
```

Navigate to `cloud.example.com` in your web browser. Click the button below the username and password fields to set up a connection to a database. Select the `MySQL/MariaDB` option. Specify an admin username and password in the first two fields. Set the database fields in accordance with the names and passwords which you set in the Docker Compose file (for the example given above, the values are `nextcloud`, `insert_password_here`, `nextcloud` and `nextcloud-db` respectively). Finally click "install" and wait until the setup is completed.

Run the following command and take a note of the values of the `IPAddress` and `IPPrefixLen` fields, as they will be required shortly.

```
docker inspect traefik
```

To perform some further configuration, open the following file for editing.

```
nano /opt/containers/nextcloud/app/config/config.php
```

Append the following lines, altering the default region and the hostname to the appropriate values. Set the values in the `trusted_proxies` field to the values of the `IPAddress` (before the slash) and `IPPrefixLen` (after the slash) fields which you noted down previously.

```
'default_phone_region' => 'DE',
'trusted_proxies' => '172.18.0.2/16',
'overwriteprotocol' => 'https',
'overwritehost' => 'cloud.example.com',
```

Also alter the contents of the following line, replacing `http` with `https`.

```
'overwrite.cli.url' => 'https://cloud.example.com',
```

Finally restart the containers.

```
docker-compose down
docker-compose up -d
```

You may now login at `cloud.example.com` using the admin credentials which you set earlier. Create a regular user and perform any further configuration in the settings menu, referring to the instructions in the official Nextcloud documentation if you require guidance.

### Maintenance

#### Performing a Manual Software Update

Keeping your Nextcloud instance up to date entails pulling the latest images listed in the table below for the corresponding tags.

|Container|Tag|
--- | --- |
|mariadb|latest|
|redis:alpine|latest|
|nextcloud|latest|

To perform a manual update, pulling the latest container image for each tag and rebuilding any out of date containers, execute the following commands.

```
docker-compose pull
docker-compose up -d --remove-orphans

# Optionally remove obsolete images
docker image prune
```

See the chapter [Automated Software Updates](chapter3-8.md) for instructions about automating the update process.

\- [start](README.md) - [previous](chapter3-6.md) - [next](chapter3-8.md) - [end](chapter4.md) -
